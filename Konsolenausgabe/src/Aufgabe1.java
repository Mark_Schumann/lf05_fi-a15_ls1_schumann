public class Aufgabe1 {
	public static void main(String[] args) {
		
		System.out.println("Aufgabe 1\n");
		
		System.out.println("Das ist ein \"Beispielsatz\".\nEin Beispielsatz ist das.");

		System.out.println("\nAufgabe 2\n");
		
		System.out.println("\t      *\n\t     ***\n\t    *****\n\t   *******\n\t  *********\n\t ***********\n\t*************\n\t     ***\n\t     ***\n\t     ***");
	
		
		System.out.println("\nAufgabe 3\n");
		
		System.out.printf("%.2f\n", 22.4234234);
		System.out.printf("%.2f\n", 111.2222);
		System.out.printf("%.2f\n", 4.0);
		System.out.printf("%.2f\n", 1000000.551);
		System.out.printf("%.2f\n", 97.34);
		
	}
}
