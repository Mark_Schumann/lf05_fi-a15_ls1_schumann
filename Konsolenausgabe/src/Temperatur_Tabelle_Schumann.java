
public class Temperatur_Tabelle_Schumann {

	public static void main(String[] args) {
		
		int i; String x = "";
		
		System.out.printf("%-12s|%10s\n","Fahrenheit", "Celsius");
		for(i = 0; i < 23; i++) {x+="-";}
		System.out.printf("%s\n", x);
		System.out.printf("%-12d|%10.2f\n", -20, -28.8889);
		System.out.printf("%-12d|%10.2f\n", -10, -23.3333);
		System.out.printf("%-12s|%10.2f\n", "+0", -17.7778);
		System.out.printf("%-12s|%10.2f\n", "+20", -6.6667);
		System.out.printf("%-12s|%10.2f\n", "+30", -1.1111);

	}

}
