/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Mark Schumann>>
  */


public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8;
   
    // Anzahl der Sterne in unserer Milchstra�e
    long anzahlSterne = 100000000000l;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3645000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    short alterTage = 7300;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 150000;
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
    int flaecheGroessteLand = 17100000;
    
    // Wie gro� ist das kleinste Land der Erde?
    
    float flaecheKleinsteLand = 0.44f;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Anzahl der Bewohner in Berlin: " + bewohnerBerlin);
    
    System.out.println("Mein Alter in tagen: " + alterTage);
    
    System.out.println("Das schwerste Tier der Welt wiegt: " + gewichtKilogramm + "kg");
    
    System.out.println("Das gr�ste Land in Quadratkilometern: " + flaecheGroessteLand);
    
    System.out.println("Das kleinste Land in Quadratkilometern: " + flaecheKleinsteLand);
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}
