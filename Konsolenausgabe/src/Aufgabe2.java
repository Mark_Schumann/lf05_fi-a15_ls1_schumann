
public class Aufgabe2 {

	public static void main(String[] args) {
		
		System.out.println("Aufgabe 1\n");
		
		System.out.printf("%5s\n","**");
		System.out.printf("%s%7s\n","*","*");
		System.out.printf("%s%7s\n","*","*");
		System.out.printf("%5s\n","**");
		
		System.out.println("\nAufgabe 2\n");
		
		for(int i=0 ; i<6; i++) {
			int x = i;
			String y = "";
			for (int j=1; j<i; j++) {
				x = x * j;
				y += j;
				if(j > 0 && j < 5) {y += " * "; }
			}
			if(i != 0){y+=i;}
			System.out.printf("%s%5s%-19s%s%4d\n", i+"!","= ",y,"=", x);
		}
		
		System.out.println("\nAufgabe 3\n");
		
		int i; String x = "";
		
		System.out.printf("%-12s|%10s\n","Fahrenheit", "Celsius");
		for(i = 0; i < 23; i++) {x+="-";}
		System.out.printf("%s\n", x);
		System.out.printf("%-12d|%10.2f\n", -20, -28.8889);
		System.out.printf("%-12d|%10.2f\n", -10, -23.3333);
		System.out.printf("%-12s|%10.2f\n", "+0", -17.7778);
		System.out.printf("%-12s|%10.2f\n", "+20", -6.6667);
		System.out.printf("%-12s|%10.2f\n", "+30", -1.1111);

	}

}
