﻿import java.util.Scanner;

class Fahrkartenautomat
{      
	public static Scanner tastatur = new Scanner(System.in);
    public static void main(String[] args)
    {    	  
    	double zuZahlenderBetrag; 
    	double eingezahlterGesamtbetrag;
       	double rückgabebetrag;
       	
    	// Preis und Anzahl der Tickets angeben
    	zuZahlenderBetrag = fahrkartenbestellungErfassen();
       
    	// Geldeinwurf
    	eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

    	// Fahrscheinausgabe
    	fahrkartenAusgeben();
       
    	// Rückgeldberechnung und -Ausgabe
    	rueckgeldAusgeben(zuZahlenderBetrag, eingezahlterGesamtbetrag);

    	System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }

	private static double fahrkartenbestellungErfassen() {
	       System.out.print("Zu zahlender Betrag (EURO): ");
	       double zuZahlenderBetrag = tastatur.nextDouble();
	       
	       System.out.print("Anzahl der Tickets: ");
	       int anzahlTickets = tastatur.nextInt();
	       
	       return zuZahlenderBetrag * anzahlTickets;
	}

	private static double fahrkartenBezahlen(double zuZahlenderBetrag) {
       	double eingeworfeneMünze;
		double eingezahlterGesamtbetrag = 0.0;
		
		while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
		{
			System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag) + " Euro");
		   	System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
		   	eingeworfeneMünze = tastatur.nextDouble();
		   	eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		
		return eingezahlterGesamtbetrag;
	}
	
	private static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}
	
	private static void rueckgeldAusgeben(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {
		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }
	}

}